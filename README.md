# Shadow reagent semantic-ui demo

<https://react.semantic-ui.com/>

## Init project

1. git clone ...
1. npm instal

### run stanalone

``` bash
npx shadow-cljs watch frontend
```

### calva repl

 calva jack-in, shadov-cljs, frontend, frontend

## Recreate package-json

``` bash
npx create-cljs-project project-name
# cd project-name
npm i --save-dev shadow-cljs
npm i semantic-ui-react
npm i react
npm i react-dom
```
