(ns app
  (:require
   [reagent.core :as r]
   [reagent.dom :as rdom]
   [semantic-ui-react :as sui]))

(defn- arc [obj]
  (r/adapt-react-class obj))

(def Button (arc sui/Button))
(def Container (arc sui/Container))
(def Segment (arc sui/Segment))
(def Sidebar (arc sui/Sidebar))
(def SidebarPushable (arc sui/SidebarPushable))
(def SidebarPusher (arc sui/SidebarPusher))

(def Icon (arc sui/Icon))
(def Image (arc sui/Image))
(def Header (arc sui/Header))
(def MenuItem (arc sui/MenuItem))

(defn- sidebar []
  (let [visible (r/atom false)
        on-click (fn [] (reset! visible (not @visible)))]
    (fn []
      [:div
       [Button {:on-click on-click} "Toggle"]
       [SidebarPushable {:as sui/Segment}
        [Sidebar {:as sui/Menu
                  :animation "push"
                  :icon "labeled"
                  :inverted true
                  :visible @visible
                  :vertical true
                  :width "thin"}
         [MenuItem {:as "a"}
          [Icon {:name "home"}]
          "HOME"]
         [MenuItem {:as "a"}
          [Icon {:name "gamepad"}]
          "Games"]]
        [SidebarPusher
         [Segment {:basic true}
          [Header {:as "h3"} "Content"]
          [Image {:src "https://react.semantic-ui.com/images/wireframe/paragraph.png"}]]]]])))


(defn mnu []
  (let [act (r/atom "home")
        click #(reset! act (.-name %2))]
    (fn []
      [:> sui/Menu {:size "tiny"}
       [:> sui/MenuItem {:name "home"
                         :active (= @act "home")
                         :on-click click}]
       [:> sui/MenuItem {:name "messages"
                         :active (= @act "messages")
                         :on-click click}]
       [:> sui/MenuMenu {:position "right"}
        [:> sui/Dropdown {:item true, :text "Language"}
         [:> sui/DropdownMenu
          [:> sui/DropdownItem "English"]
          [:> sui/DropdownItem "Russian"]
          [:> sui/DropdownItem "Spanish"]]]
        [:> sui/MenuItem
         [:> sui/Button {:primary true} "Sign Up"]]]])))

(defn- main-component []
  [:<>
   [Container
    [mnu]]
   [:> sui/Divider]
   [Container
    [sidebar]]
   [:div.ui.segment
    [:h3 "Buttons"]
    [:div.large.ui.basic.buttons
     (for [title ["a" "b" "c"]]
       [:button.ui.button title])]]])

(defn ^:export init []
  (rdom/render [main-component] (js/document.getElementById "root")))

(defn ^:export refresh []
  (js/console.log "refresh!")
  (init))

(defn ^:dev/before-load stop []
  (js/console.log "stop!"))

(defn ^:dev/after-load start []
  (js/console.log "start!"))
